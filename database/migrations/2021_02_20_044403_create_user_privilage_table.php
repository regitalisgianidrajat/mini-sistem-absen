<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPrivilageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_privileges', function (Blueprint $table) {
            $table->id();
            $table->integer('role_id');
            $table->string('menu');
            $table->enum('view',[0,1])->default(0)->comment('0=false | 1 true');
            $table->enum('create',[0,1])->default(0)->comment('0=false | 1 true');
            $table->enum('edit',[0,1])->default(0)->comment('0=false | 1 true');
            $table->enum('delete',[0,1])->default(0)->comment('0=false | 1 true');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_privileges');
    }
}
