<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserRolesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UserRoleModel::create(
            [
                'role_name' => 'admin'
            ]
        );
        \App\Models\UserRoleModel::create(
            [
                'role_name' => 'employee'
            ]
        );

    }
}
