<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserPrivilegezSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UserPrivilegeModel::create([
            'role_id' => 1,
            'menu' => 'employee',
            'view' => 1,
            'create' => 1,
            'edit' => 1,
            'delete' => 1
        ]);
        \App\Models\UserPrivilegeModel::create([
            'role_id' => 1,
            'menu' => 'departement',
            'view' => 1,
            'create' => 1,
            'edit' => 1,
            'delete' => 1
        ]);
        \App\Models\UserPrivilegeModel::create([
            'role_id' => 1,
            'menu' => 'attendance',
            'view' => 1
        ]);
        \App\Models\UserPrivilegeModel::create([
            'role_id' => 2,
            'menu' => 'attendance',
            'view' => 1,
            'create' => 1,
            'edit' => 1,
            'delete' => 1
        ]);
    }
}
