<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UserModel::create(
            [
                'departement_id' => 1,
                'role_id' => 1,
                'fullname' => 'admin',
                'email' => 'admin@mail.com',
                'password' => Hash::make('admin'),
                'status' => 'active'
            ]
        );
        \App\Models\UserModel::create(
            [
                'departement_id' => 1,
                'role_id' => 2,
                'fullname' => 'employee 1',
                'email' => 'employee1@mail.com',
                'password' => Hash::make('employee1'),
                'status' => 'active'
            ]
        );
        \App\Models\UserModel::create([
                'departement_id' => 2,
                'role_id' => 2,
                'fullname' => 'employee 2',
                'email' => 'employee2@mail.com',
                'password' => Hash::make('employee2'),
                'status' => 'active'
            ]
        );

    }
}
