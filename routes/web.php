<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('/user/login', 'API\UserController@login');

    $router->group(['middleware' => 'user.auth'], function () use ($router) {
        $router->get('/employee', 'API\EmployeeController@index');
        $router->get('/employee/detail', 'API\EmployeeController@show');
        $router->post('/employee', 'API\EmployeeController@store');
        $router->put('/employee', 'API\EmployeeController@update');
        $router->delete('/employee', 'API\EmployeeController@destroy');

        $router->get('/departement', 'API\DepartementController@index');
        $router->get('/departement/detail', 'API\DepartementController@show');
        $router->post('/departement', 'API\DepartementController@store');
        $router->put('/departement', 'API\DepartementController@update');
        $router->delete('/departement', 'API\DepartementController@destroy');
        
        $router->get('/attendance', 'API\AttendanceController@index');
        $router->post('/attendance', 'API\AttendanceController@store');
    });

});