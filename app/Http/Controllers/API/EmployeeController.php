<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel;
use App\Traits\GeneralServices;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    use GeneralServices;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = UserModel::select('*')->where('role_id',2)->with(['departement','role'])->get();	
		if (!$getData->isEmpty()) {
			return $this->ResponseJson(200,"Employee List",$getData);
		}else{
			return $this->ResponseJson(404,"Employee Not Found",array());
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'departement_id' => 'required|integer',
            'fullname' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'password_confirmation ' => 'string',
            'gender' => 'nullable',
            'status' => 'required|in:active,inactive',
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $request['password'] = hash::make($request['password']);
        $request['role_id'] = 2;
        $save = UserModel::create($request->except(['_method','password_confirmation']));
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Employee succesfully added",$save);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $getData = UserModel::select('*')->where('id',$request->id)->with(['departement','role'])->first();	
		if (!empty($getData)) {
			return $this->ResponseJson(200,"Employee Detail",$getData);
		}else{
			return $this->ResponseJson(404,"Employee Not Found",array());
		}
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'departement_id' => 'required|integer',
            'fullname' => 'required|string',
            'gender' => 'nullable',
            'status' => 'required|in:active,inactive',
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = UserModel::where('id',$request->id)->update($request->except(['_method','id']));
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Employee succesfully updated",$save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'Required|integer'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $delete = UserModel::where('id',$request->id)->delete();
        if(!$delete){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Employee succesfully deleted",$delete);
    }
}
