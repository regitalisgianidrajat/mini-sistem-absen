<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserAttendanceModel;
use App\Traits\GeneralServices;
use Illuminate\Support\Facades\Hash;

class AttendanceController extends Controller
{
    use GeneralServices;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = UserAttendanceModel::select('*')->with(['user'])->get();	
		if (!$getData->isEmpty()) {
            $getData = $getData->map(function($key){
				if($key->type == 1){
					$key->type ="Check-Out";
				}else{
                    $key->type ="Check-In";
                }
				return $key;
			});
			return $this->ResponseJson(200,"Attendance List",$getData);
		}else{
			return $this->ResponseJson(404,"Attendance Not Found",array());
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $rules = [
            'user_id' => 'required|integer',
            'comment ' => 'nullable|string',
            'remark' => 'required|in:present,absent',
            'type' => 'required|in:0,1',
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        // Find the member by email
        $cekData = UserAttendanceModel::select('*')->where('user_id','=',$request['user_id'])
                    ->where('type','=',$request['type'])->whereDate('created_at','=',date('Y-m-d'))->get();
            
        if (!$cekData->isEmpty()) {
                return $this->ResponseJson(406, 'Failed! You already submit the attendance');
        }
        $save = UserAttendanceModel::create($request->all());
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Attendance succesfully added",$save);
    
    }
}
