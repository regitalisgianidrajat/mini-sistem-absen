<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DepartementModel;
use App\Traits\GeneralServices;

class DepartementController extends Controller
{
    use GeneralServices;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getData = DepartementModel::select('*')->get();	
		if (!$getData->isEmpty()) {
			return $this->ResponseJson(200,"Divisi",$getData);
		}else{
			return $this->ResponseJson(404,"Divisi Not Found",array());
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'departement_name' => 'Required|string',
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = DepartementModel::create($request->all());
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Divisi succesfully added",$save);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $getData = DepartementModel::select('*')->where('departement_id',$request->departement_id)->first();	
		if (!empty($getData)) {
			return $this->ResponseJson(200,"Divisi Detail",$getData);
		}else{
			return $this->ResponseJson(404,"Divisi Not Found",array());
		}
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'departement_name' => 'Required|string',
            'departement_id' => 'Required|integer'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $save = DepartementModel::where('departement_id',$request->departement_id)->update($request->except(['_method','departement_id']));
        if(!$save){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Divisi succesfully updated",$save);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'departement_id' => 'Required|integer'
        ];
        $ValidateData = $this->ValidateRequest($request->all(), $rules);

        if (!empty($ValidateData)) {
            return $ValidateData;
        }
        $delete = DepartementModel::where('departement_id',$request->departement_id)->delete();
        if(!$delete){
            return $this->ResponseJson(406,"Server Error!");
        } 
        return $this->ResponseJson(200,"Divisi succesfully deleted",$delete);
    }
}
