<?php

namespace app\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\UserModel;
use App\Traits\GeneralServices;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use GeneralServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        $PostRequest = $request->only(
                'email',
                'password'
        );

        $role = [
            'email' => 'Required',
            'password' => 'Required',
        ];

        $validateData = $this->ValidateRequest($PostRequest, $role);

        if (!empty($validateData)) {
            return $validateData;
        }
        
            // Find the member by email
            $cek_member = UserModel::select('*')->where('email','=',$request['email'])->with(['departement','role'])->first();
            
            if (empty($cek_member)) {
                    return $this->ResponseJson(406, 'Failed! Email Address Not Found!');
            }
            $cek_member->makeVisible('password');
            if (Hash::check($request['password'], $cek_member->password)) {
                if ($cek_member->status=="active") {
                    $cek_member['credential ']      = $this->GenerateToken($cek_member);
                    return $this->ResponseJson(200, 'Login Success!', $cek_member->makeHidden('password'));
                }else{
                    return $this->ResponseJson(400, 'Failed! Member inactive!');
                }
            }
            return $this->ResponseJson(400, 'Failed! invalid password!');
        
    }

}
