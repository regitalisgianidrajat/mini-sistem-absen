<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Firebase\JWT\JWT;
use DateTime;

trait GeneralServices {
    public function GenerateToken($credential) {
        
		$key = 'X-Api-Key';
		$date = new DateTime();
		$token['data'] = ['id' => $credential->user_id];
		$token['iat'] = $date->getTimestamp();
		$token['exp'] = $date->getTimestamp() + 86400*1; // a day 
		return JWT::encode($token, $key);
    }

    public function ResponseJson($status,$message,$data = null){
		if($status == 200){
			$response = [
				'status' => true,
				'message' => $message,
				'data' => $data
			];
		}else{
			$response = [
				'status' => false,
				'message' => $message
			];
		}
		return response()->json($response, 200);
	}
    function ValidateRequest($params,$rules){

		$validator = Validator::make($params, $rules);

		if ($validator->fails()) {
			$response = [
				'status' => false,
				// 'message' => $validator->messages()
				'message' =>  $validator->errors()->first()
			];
			return response()->json($response, 200);
		}
	}   
}