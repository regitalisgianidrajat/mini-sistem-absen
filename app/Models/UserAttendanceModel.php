<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserAttendanceModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table   = 'user_attendances';
	public $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = [
        'user_id','type','remark','comment'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public function user()
    {
        return $this->belongsTo('App\Models\UserModel', 'user_id', 'id');
    }
}
