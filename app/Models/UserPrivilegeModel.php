<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserPrivilegeModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table   = 'user_privileges';
	public $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'role_id','menu','view','create','edit','delete'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];
    public function role()
    {
        return $this->belongsTo('App\Models\UserRole', 'role_id', 'role_id');
    }
}
