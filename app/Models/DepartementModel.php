<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DepartementModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $table   = 'departement';
	public $primarykey = 'departement_id';
    public $timestamps = true;

    protected $fillable = [
        'departement_name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at','updated_at'
    ];
    public function users()
    {
        return $this->hasMany('App\Models\UserModel', 'departement_id', 'departement_id');
    }
}
