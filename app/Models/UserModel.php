<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserModel extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $table   = 'users';
	public $primarykey = 'id';
    public $timestamps = true;

    protected $fillable = [
        'departement_id','role_id','fullname', 'email','password','photo','gender','status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','deleted_at','updated_at'
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\UserRoleModel', 'role_id', 'role_id')->with('privileges');
    }
    public function departement()
    {
        return $this->belongsTo('App\Models\DepartementModel', 'departement_id', 'departement_id');
    }
    public function attendace()
    {
        return $this->hasMany('App\Models\UserAttendanceModel', 'user_id', 'id');
    }

}
